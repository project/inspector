<?php

namespace Drupal\inspector\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Inspector routes.
 */
class InspectorOverviewController extends ControllerBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->themeHandler = $theme_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler'),
      $container->get('theme_handler')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {
    $all_cfg = $this->configFactory->listAll();
    // todo: make the JS hierarchy() function handle explicit keys, which also
    // result from resolving the 'parents' in JS.
    $ignored_keys = [
      'system.image',
      'system.theme',
      // The umami profile is included under 'module' dependencies, @todo: add
      // support for that.
      'tour.tour.umami-front',
    ];
    $all_cfg = array_values(array_diff($all_cfg, $ignored_keys));

    $cfg_deps = array_map(function ($cfg_key) {
      $deps = array_diff_key($this->configFactory->get($cfg_key)->get('dependencies') ?? [], array_flip(['enforced', 'content']));
      $deps_prefixed = [];
      foreach ($deps as $dep_group_key => $dep_group) {
        foreach ($dep_group as $dep_item) {
          $deps_prefixed[] = 'site:' . $dep_group_key . ':' . $dep_item;
        }
      }
      return [
        'name' => 'site:config:' . $cfg_key,
        'imports' => $deps_prefixed,
      ];
    }, $all_cfg);

    $module_handler = \Drupal::moduleHandler();
    $all_modules = array_filter(
      \Drupal::service('extension.list.module')->getList(),
      function ($extension) {
        return $extension->getType() == 'module' && $extension->status;
      }
    );
    $module_deps = array_map(function ($module_ext) {
      return [
        'name' => 'site:module:' . $module_ext->getName(),
        'imports' => array_map(function ($module_dep) {
          return 'site:module:' . $module_dep;
        }, array_keys($module_ext->requires) ?? []),
      ];
    }, array_values($all_modules));

    $all_themes = \Drupal::service('theme_handler')->listInfo();
    $theme_deps = array_map(function ($theme_ext) {
      return [
        'name' => 'site:theme:' . $theme_ext->getName(),
        'imports' => array_map(function ($theme_dep) {
          return 'site:theme:' . $theme_dep;
        }, array_keys($theme_ext->base_themes ?? []) ?? []),
      ];
    }, array_values($all_themes));

    $inspector_data = [
      'deps' => array_merge($cfg_deps, $module_deps, $theme_deps),
    ];

    $build['content'] = [
      '#type' => 'item',
      '#markup' => '',
      '#prefix' => '<div id="inspector-overview">',
      '#suffix' => '</div>',
      '#attached' => [
        'drupalSettings' => [
          'inspector' => $inspector_data,
        ],
        'library' => [
          'inspector/inspector',
        ],
      ],
    ];

    return $build;
  }

}
