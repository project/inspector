/**
 * @file
 * Inspector behaviors.
 */
(function ($, Drupal) {

  'use strict';

const hierarchy_match_rules = [
  'site:',
  'site:module:',
  'site:theme:',
  'site:config:',
  'site:config:block.block.',
  'site:config:block_content.type.',
  'site:config:comment.type.',
  'site:config:core.base_field_override.node.',
  'site:config:core.entity_form_display.block_content.',
  'site:config:core.entity_form_display.comment.',
  'site:config:core.entity_form_display.node.',
  'site:config:core.entity_form_display.taxonomy_term.',
  'site:config:core.entity_form_display.user.',
  'site:config:core.entity_form_display.',
  'site:config:core.entity_view_display.block_content.',
  'site:config:core.entity_view_display.comment.',
  'site:config:core.entity_view_display.node.',
  'site:config:core.entity_view_display.taxonomy_term.',
  'site:config:core.entity_view_display.user.',
  'site:config:core.entity_view_display.',
  'site:config:core.entity_view_mode.block_content.',
  'site:config:core.entity_view_mode.comment.',
  'site:config:core.entity_view_mode.node.',
  'site:config:core.entity_view_mode.taxonomy_term.',
  'site:config:core.entity_view_mode.user.',
  'site:config:core.entity_view_mode.',
  'site:config:core.entity_form_mode.block_content.',
  'site:config:core.entity_form_mode.comment.',
  'site:config:core.entity_form_mode.node.',
  'site:config:core.entity_form_mode.taxonomy_term.',
  'site:config:core.entity_form_mode.user.',
  'site:config:core.entity_form_mode.',
  'site:config:contact.form.',
  'site:config:core.date_format.',
  'site:config:editor.editor.',
  'site:config:field.field.block_content.',
  'site:config:field.field.comment.',
  'site:config:field.field.node.',
  'site:config:field.field.user.',
  'site:config:field.field.',
  'site:config:field.storage.block_content.',
  'site:config:field.storage.comment.',
  'site:config:field.storage.node.',
  'site:config:field.storage.user.',
  'site:config:field.storage.',
  'site:config:filter.format.',
  'site:config:image.style.',
  'site:config:node.type.',
  'site:config:rdf.mapping.block_content.',
  'site:config:rdf.mapping.comment.',
  'site:config:rdf.mapping.node.',
  'site:config:rdf.mapping.taxonomy_term.',
  'site:config:rdf.mapping.user.',
  'site:config:rdf.mapping.',
  'site:config:search.page.',
  'site:config:system.action.',
  'site:config:system.menu.',
  'site:config:system.theme.',
  'site:config:tour.tour.',
  'site:config:taxonomy.vocabulary.',
  'site:config:user.role.',
  'site:config:views.view.',
].sort().reverse();

const buildChordDiagram = function () {
  // Code adapted from https://observablehq.com/@d3/hierarchical-edge-bundling
  const width = 1954;
  const radius = width / 2;
  let tree = d3.cluster()
    .size([2 * Math.PI, radius - 250]);
  const line = d3.lineRadial()
    .curve(d3.curveBundle.beta(0.85))
    .radius(d => d.y)
    .angle(d => d.x);
  const colornone = "#ccc";
  const colorout = "#f00";
  const colorin = "#00f";

  function label(node) {
    return node.data.name
      .replaceAll('site:config:', '')
      .replaceAll('site:module:', '')
      .replaceAll('site:theme:', '');
  }

  function bilink(root) {
    const map = new Map(root.leaves().map(d => [d.data.name, d]));
    for (const d of root.leaves()) d.incoming = [], d.outgoing = d.data.imports.map(i => [d, map.get(i)]);
    for (const d of root.leaves()) for (const o of d.outgoing) o[1].incoming.push(o);
    return root;
  }

  function parentName(name) {
    for (const match_prefix of hierarchy_match_rules) {
      if (name.indexOf(match_prefix) == 0) {
        return match_prefix.slice(0, -1);
      }
    }
    return false;
  }

  function hierarchy(data, delimiter = ":") {
    let root;
    const map = new Map;
    data.forEach(function find(data) {
      const {name} = data;
      console.log('Finding for: ' + name);
      if (map.has(name)) return map.get(name);
      const parent_name = parentName(name);
      map.set(name, data);
      if (parent_name) {
        console.log('Recursing for: ' + parent_name);
        find({name: parent_name, children: []}).children.push(data);
      } else {
        root = data;
      }
      return data;
    });
    return root;
  }

  let data = hierarchy(drupalSettings.inspector.deps);
  console.log(data);

  const root = tree(bilink(d3.hierarchy(data)
      .sort((a, b) => d3.ascending(a.height, b.height) || d3.ascending(a.data.name, b.data.name))));

  const svg = d3.create("svg")
      .attr("viewBox", [-width / 2, -width / 2, width, width]);

  const f_size = d3.scaleLinear()
    .domain([150, 400])
    .range([20, 10]);

  const node = svg.append("g")
      .attr("font-family", "sans-serif")
      .attr("font-size", f_size(root.leaves().length))
    .selectAll("g")
    .data(root.leaves())
    .join("g")
      .attr("transform", d => `rotate(${d.x * 180 / Math.PI - 90}) translate(${d.y},0)`)
    .append("text")
      .attr("dy", "0.31em")
      .attr("x", d => d.x < Math.PI ? 6 : -6)
      .attr("text-anchor", d => d.x < Math.PI ? "start" : "end")
      .attr("transform", d => d.x >= Math.PI ? "rotate(180)" : null)
      .text(d => label(d))
      .each(function(d) { d.text = this; })
      .on("mouseover", overed)
      .on("mouseout", outed)
      .on("click", clicked)
      .call(text => text.append("title").text(d => `${label(d)}
  ${d.outgoing?.length} dependencies
  ${d.incoming?.length} dependants`));

  const link = svg.append("g")
      .attr("stroke", colornone)
      .attr("fill", "none")
    .selectAll("path")
    .data(root.leaves().flatMap(leaf => leaf.outgoing))
    .join("path")
      .style("mix-blend-mode", "multiply")
      .attr("d", ([i, o]) => line(i.path(o)))
      .each(function(d) { d.path = this; });
  console.log(root);

  const dots = svg.append("g")
    .attr("stroke", "transparent")
    .attr("fill", colornone)
  .selectAll("circle")
  .data(root.leaves())
  .join("circle")
    .attr("stroke", 0)
    .attr("r", 4)
    .attr("transform", d => `rotate(${d.x * 180 / Math.PI - 90}) translate(${d.y},0)`)
    .each(function (n) { n.circle = this; })
    .on("mouseover", overed)
    .on("mouseout", outed)
    .on("click", clicked)
    .call(circle => circle.append("title").text(d => `${label(d)}
  ${d.outgoing?.length} dependencies
  ${d.incoming?.length} dependants`));

  root.each(n => {
    if (n.height == 0) {
      n.selected = false;
    }
  })

  function overed(event, d) {
    if (!d.incoming) return;
    link.style("mix-blend-mode", null);
    d3.selectAll([d.text, d.circle]).attr("fill", 'black').attr("font-weight", "bold").raise();

    d3.selectAll(d.incoming.map(d => d.path)).attr("stroke", colorin).raise();
    d3.selectAll(d.incoming.map(([d]) => d.text)).attr("fill", colorin).attr("font-weight", "bold");
    d3.selectAll(d.incoming.map(([d]) => d.circle)).attr("fill", colorin);
    d3.selectAll(d.outgoing.map(d => d.path)).attr("stroke", colorout).raise();
    d3.selectAll(d.outgoing.map(([, d]) => d.text)).attr("fill", colorout).attr("font-weight", "bold");
    d3.selectAll(d.outgoing.map(([, d]) => d.circle)).attr("fill", colorout);
  }

  function outed(event, d) {
    if (!d.incoming) return;
    if (selected.length == 0) {
      link.style("mix-blend-mode", "multiply");
    }
    d3.selectAll([d.text, d.circle]).attr("fill", null).attr("font-weight", null).raise();

    d3.selectAll(d.incoming.map(d => d.path)).attr("stroke", null);
    d3.selectAll(d.incoming.map(([d]) => d.text)).attr("fill", null).attr("font-weight", null);
    d3.selectAll(d.incoming.map(([d]) => d.circle)).attr("fill", null);
    d3.selectAll(d.outgoing.map(d => d.path)).attr("stroke", null);
    d3.selectAll(d.outgoing.map(([, d]) => d.text)).attr("fill", null).attr("font-weight", null);
    d3.selectAll(d.outgoing.map(([, d]) => d.circle)).attr("fill", null);

    persistHighlight();
  }

  function persistHighlight() {
    for (const n of selected) {
      d3.selectAll([n.text, n.circle]).attr("fill", 'black').attr("font-weight", "bold").raise();

      d3.selectAll(n.incoming.map(n => n.path)).attr("stroke", colorin).raise();
      d3.selectAll(n.incoming.map(([n]) => n.text)).attr("fill", colorin).attr("font-weight", "bold");
      d3.selectAll(n.incoming.map(([n]) => n.circle)).attr("fill", colorin);
      d3.selectAll(n.outgoing.map(n => n.path)).attr("stroke", colorout).raise();
      d3.selectAll(n.outgoing.map(([, n]) => n.text)).attr("fill", colorout).attr("font-weight", "bold");
      d3.selectAll(n.outgoing.map(([, n]) => n.circle)).attr("fill", colorout);
    }
  }

  let selected = [];
  function clicked(event, n) {
    console.log("event", event);
    console.log("n", n);
    n.selected = !n.selected;
    if (n.selected) {
      selected.push(n);
    }
    else {
      selected = selected.filter(v => v.data.name != n.data.name);
    }
  }

  console.log(svg.node());
  $(svg.node()).appendTo('#inspector-overview');
};

  /**
   * Generates the overview view.
   */
  Drupal.behaviors.inspector_overview = {
    build: buildChordDiagram,
    attach: function (context, settings) {
      if ($(document).once('inspector-overview').length == 0) return;

      this.build();
    }
  };

} (jQuery, Drupal));
