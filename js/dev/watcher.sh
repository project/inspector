#!/bin/bash

MONITORDIR="$(pwd)"
while inotifywait -qq --exclude=".(swp|txt|sh)" -e modify "${MONITORDIR}"; do
  echo $(date) | tee $MONITORDIR/last-changed.txt
  cp -u inspector.js ../inspector.js
done
