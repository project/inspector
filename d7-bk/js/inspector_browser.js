angular.module('Inspector', ['ui.bootstrap'])
  .controller('InspectorBrowser', ['$scope', '$http', function($scope, $http) {
    $scope.component = false;
    $scope.componentSearch = function(value) {
      return $http.get(Drupal.settings.inspector.search_path, {
        params: {
          s: value,
        }
      }).then(function(res){
        var components = [];
        angular.forEach(res.data, function(item){
          components.push(item);
        });
        return components;
      });
    };

    $scope.getComponent = function(component_name) {
      var mScope = $scope;
      return $http.get(Drupal.settings.inspector.get_path + '/' + component_name)
        .then(function(res) {
          mScope.component = res.data;
        })
    }

    $scope.setActiveComponent = function(component) {
      $scope.selectedComponent = component.cid;
      $scope.getComponent(component.cid);
    }
  }])
