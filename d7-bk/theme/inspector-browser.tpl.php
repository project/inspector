<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html>
<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body>
  <div ng-app="Inspector">
    <div ng-controller="InspectorBrowser">
      <script type="text/ng-template" id="customTemplate.html">
        <a>
          <h5 bind-html-unsafe="match.model.component_name | typeaheadHighlight:query"></h5>
          <h6 bind-html-unsafe="match.model.cid | typeaheadHighlight:query"></h6>
        </a>
      </script>
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="input-group input-group-lg">
              <span class="input-group-addon">@</span>
              <input class="form-control" type="text" ng-model="selectedComponent" typeahead="component.cid for component in componentSearch($viewValue) | filter:$viewValue" typeahead-min-length="4" typeahead-template-url="customTemplate.html" />
              <span class="input-group-btn"><button class="btn btn-default" ng-click="getComponent(selectedComponent)">Search</button></span>
            </div>
          </div>
          <div class="col-lg-6">
            <a href="<?php print $go_back_url; ?>"><?php print t('Go back'); ?></a>
          </div>
        </div>
        <div class="component-details row" ng-show="component">
          <div class="dependants col-lg-12">
            <h2>Dependants:</h2>
            <ul>
              <li ng-repeat="item in component.dependants" ng-click="setActiveComponent(item)">{{item.component_name}}</li>
            </ul>
          </div>
          <div class="component col-lg-12">
            <h2>Component</h2>
            <h3>{{component.component_name}}</h3>
            <h4>{{component.cid}}</h4>
          </div>
          <div class="dependencies col-lg-12">
            <h2>Dependencies</h2>
            <ul>
              <li ng-repeat="item in component.dependencies" ng-click="setActiveComponent(item)">{{item.component_name}}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php print $page_bottom; ?>
</body>
</html>
